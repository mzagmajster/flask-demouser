from distutils.core import setup

setup(
	name='Flask-demouser',
	version='0.1',
	packages=['flask_demouser'],
	url='http://maticzagmajster.ddns.net/',
	license='',
	author='Matic Zagmajster',
	author_email='zagm101@gmail.com',
	description='Enable demo user in application.'
)
