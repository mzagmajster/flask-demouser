from datetime import datetime, timedelta
from sqlalchemy import Column, DateTime, Boolean
from sqlalchemy.ext.declarative import declared_attr


class DemoUser(object):
	@declared_attr
	def demo(cls):
		return Column(Boolean)

	@declared_attr
	def demo_expires(cls):
		return Column(DateTime)

	def is_demo(self):
		return self.demo

	def has_demo_expired(self):
		return datetime.utcnow() > self.demo_expires

	def set_demo_expiration(self, d_obj):
		if isinstance(d_obj, timedelta):
			self.demo_expires = self.demo_expires + d_obj
		else:
			self.demo_expires = d_obj
